# TripBuilder
By Alex Kua

## Getting Started

TripBuilder is a web service (API) that serves as the engine for front-end websites to manage trips for their customers.

The following calls are available from the website:
1. Alphabetical listing of real airports in the world
2. List of flights for a trip
3. Adding flights to a trip
4. Removing flights from a trip

The application was built in PHP with the **Laravel** framework. To install, download the files, setup a PHP web server and point your server towards the **public** folder.

You have to install composer (https://getcomposer.org/) and run this command on the root folder:

To setup database credentials, you have to modify the **.env** file in the root folder.

`php composer install`

I used Laravel migrations and seeders to create the database tables and to populate the fake data. You can do both in one command by going to the root folder of the project in your command line and type:

`php artisan migrate:refresh --seed`

The website is a single-page website used to test the API calls. For testing purposes, API calls are public, so they do not require any type of authentication. Please refer to the API documentation below for possible API calls.

### Code explanation

3 models have been created for this application: **Airport, Trip and Flight**

A trip goes from an airport to another airport (ex: From YUL to ATL). A trip can have many flights and a flight goes also from an aiport to another airport. A trip can have many flights before reaching it's final destination (ex: Trip from YUL to ATL has 2 flights: From YUL to LAX and from LAX to ATL)

### API documentation

| API CALL               | TYPE    | DATA | RESULT |
| ---------------------- | ------- | ------------------ | ------ |
| /trips                 | GET     | | Get all trips|
| /trip/{tripId}/flights | GET     | tripId: id of the trip | Get all flights for a trip  |
| /flights               | GET     | | Get all flights  |
| /airports              | GET     | | Get all airports (alphabetical order)   |
| /trip/{tripId}/flight  | POST    | POST: {fromAirportId: departure airport id, toAirportId: arrival airport id} | Add flight to a trip   |
| flight/{flightId}      | DELETE  |  flightId: id of the flight | Remove fligh |

### Demo

There is a demo available on http://tripbuilder.alexkua.com/