<?php

use Illuminate\Database\Seeder;
use App\Trip;

class TripsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Trip::truncate();
      $trips = [
        [
          "current_airport_id"     => 1,
          "destination_airport_id" => 3,
          "flight_time"            => "2018-02-20 12:00:00"
        ],
        [
          "current_airport_id"     => 4,
          "destination_airport_id" => 5,
          "flight_time"            => "2018-02-21 16:00:00"
        ],
        [
          "current_airport_id"     => 5,
          "destination_airport_id" => 6,
          "flight_time"            => "2018-02-22 16:00:00"
        ],
        [
          "current_airport_id"     => 4,
          "destination_airport_id" => 7,
          "flight_time"            => "2018-02-22 21:00:00"
        ],
        [
          "current_airport_id"     => 7,
          "destination_airport_id" => 1,
          "flight_time"            => "2018-02-23 10:00:00"
        ]
      ];
      foreach ($trips as $trip) {
        Trip::create($trip);
      }
    }
}
