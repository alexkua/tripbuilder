<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AirportsSeeder::class);
        $this->call(FlightsSeeder::class);
        $this->call(TripsSeeder::class);
    }
}
