<?php

use Illuminate\Database\Seeder;
use App\Flight;

class FlightsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Flight::truncate();
      $flights = [
        [
          "trip_id"                => 1,
          "current_airport_id"     => 1,
          "destination_airport_id" => 2
        ],
        [
          "trip_id"                => 1,
          "current_airport_id"     => 2,
          "destination_airport_id" => 3
        ],
        [
          "trip_id"                => 2,
          "current_airport_id"     => 4,
          "destination_airport_id" => 5
        ]
      ];
      foreach ($flights as $flight) {
        Flight::create($flight);
      }
    }
}
