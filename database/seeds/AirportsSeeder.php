<?php

use Illuminate\Database\Seeder;
use App\Airport;

class AirportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Airport::truncate();

        $airports = [
          [
            "name" => "Hartsfield–Jackson Atlanta International Airport",
            "location" => "Atlanta, Georgia",
            "country" => "United States",
            "code" => "ATL"
          ],
          [
            "name" => "Beijing Capital International Airport",
            "location" => "Chaoyang-Shunyi, Beijing",
            "country" => "China",
            "code" => "PEK"
          ],
          [
            "name" => "Dubai International Airport",
            "location" => "Garhoud, Dubai",
            "country" => "United Arab Emirates",
            "code" => "DXB"
          ],
          [
            "name" => "Los Angeles International Airport",
            "location" => "Los Angeles, California",
            "country" => "United States",
            "code" => "LAX"
          ],
          [
            "name" => "Tokyo Haneda International Airport",
            "location" => "Ōta, Tokyo",
            "country" => "Japan",
            "code" => "HND"
          ],
          [
            "name" => "Toronto Pearson International Airport",
            "location" => "Mississauga, Ontario",
            "country" => "Canada",
            "code" => "YYZ"
          ],
          [
            "name" => "Montréal–Pierre Elliott Trudeau International Airport",
            "location" => "Montreal, Quebec",
            "country" => "Canada",
            "code" => "YUL"
          ]
        ];
        foreach ($airports as $airport) {
          Airport::create($airport);
        }
    }
}
