<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>TripBuilder by Alex Kua</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

        <!-- Bootstrap CDN -->
        <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100px;
                margin: 0;
            }
            #title {
              margin-top: 30px;
              font-size: 60px;
              text-align: center;
              width: 100%;
            }
            .container {
              padding:50px;
              text-align:center;
            }
            .moreInfoContainer {
              display:none;
              width:500px;
              margin:10px auto;
            }
            #results {
              margin:10px 0px;
              background-color:#272822;
              padding:10px;
              border-radius:10px;
              display:none;
              text-align:left;
            }
            #results pre {
              color:#F8F8F2;
            }
        </style>
    </head>
    <body>
        <div id="title">
          TripBuilder
        </div>
        <div class="container">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="apiTester">Test the API: </label>
            </div>
            <select class="custom-select" id="apiTester">
              <option value="" selected>Choose...</option>
              <option value="list_airports">Alphabetical listing of real airports in the world</option>
              <option value="list_flights">List of flights for a trip</option>
              <option value="add_flight">Adding flights to a trip</option>
              <option value="remove_flight">Removing flights from a trip</option>
            </select>
          </div>
          <!-- List flights container -->
          <div id="list_flights_container" class="moreInfoContainer">
            <div class="form-group">
              <label for="list_flights_container_trips">Please specify for which trip you wish to view the flights:</label>
              <select class="form-control" id="list_flights_container_trips">
              </select>
            </div>
          </div>
          <!-- Add flight container -->
          <div id="add_flight_container" class="moreInfoContainer">
            <div class="form-group">
              <label>
                Please specify the departure airport, the arrival airport and the trip:
              </label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="add_flight_container_departure">
                    Departure airport: 
                  </label>
                </div>
                <select class="custom-select" id="add_flight_container_departure">
                </select>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="add_flight_container_arrival">
                    Arrival airport:
                  </label>
                </div>
                <select class="custom-select" id="add_flight_container_arrival">
                </select>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="add_flight_container_trip">
                    Trip:
                  </label>
                </div>
                <select class="custom-select" id="add_flight_container_trip">
                </select>
              </div>
            </div>
          </div>
          <!-- Remove flight container -->
          <div id="remove_flight_container" class="moreInfoContainer">
            <div class="form-group">
              <label>
                Please specify the flight you wish to remove:
              </label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="remove_flight_container_trip">
                    Choose a trip:
                  </label>
                </div>
                <select class="custom-select" id="remove_flight_container_trip">
                </select>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="remove_flight_container_flight">
                    Choose a flight:
                  </label>
                </div>
                <select class="custom-select" id="remove_flight_container_flight">
                </select>
              </div>
            </div>
          </div>
          <button id="sendRequest" type="button" class="btn btn-light">Send request</button>
          <div id="results">
          </div>
        </div>
    </body>
    <script>
      (function($) {
        $(document).ready(function() {
          initEvents();
        });
        function initEvents() {
          // API select on change event
          $("#apiTester").on('change', function() {
            var val = $(this).val();
            $(".moreInfoContainer").hide();
            $("#results").hide();
            if (val == "") {
              return;
            }
            if ($("#" + val + "_container").length) {
              $("#" + val + "_container").show();
              if (val == "list_flights") {
                // Add list flight inputs
                $.getJSON("/trips", function(data) {
                  $("#list_flights_container_trips").html("");
                  for (var i = 0; i < data.length; i++) {
                    $('#list_flights_container_trips').append($('<option>', {
                      value: data[i].id,
                      text: "From " + data[i].from_code + " to " + data[i].to_code + " (" + data[i].flight_time + ")"
                    }));
                  }
                });
              } else if (val == "add_flight") {
                // Fill add flight inputs
                var airports = [];
                var trips = [];
                $.when(
                  $.getJSON("/airports", function(data) {
                    airports = data;
                  }),
                  $.getJSON("/trips", function(data) {
                    trips = data;
                  })
                ).then(function() {
                  $("#add_flight_container_departure").html("");
                  $("#add_flight_container_arrival").html("");
                  for (var i = 0; i < airports.length; i++) {
                    $('#add_flight_container_departure').append($('<option>', {
                      value: airports[i].id,
                      text: airports[i].name + " (" + airports[i].code + ")"
                    }));
                    $('#add_flight_container_arrival').append($('<option>', {
                      value: airports[i].id,
                      text: airports[i].name + " (" + airports[i].code + ")"
                    }));
                  }
                  $("#add_flight_container_trip").html("");
                  for (var i = 0; i < trips.length; i++) {
                    $('#add_flight_container_trip').append($('<option>', {
                      value: trips[i].id,
                      text: "From " + trips[i].from_code + " to " + trips[i].to_code + " (" + trips[i].flight_time + ")"
                    }));
                  }
                });
              } else if (val == "remove_flight") {
                // Remove flight
                $("#remove_flight_container_flight").html("");
                $.getJSON("/trips", function(data) {
                  $("#remove_flight_container_trip").html('<option value="">Choose...</option>');
                  for (var i = 0; i < data.length; i++) {
                    $('#remove_flight_container_trip').append($('<option>', {
                      value: data[i].id,
                      text: "From " + data[i].from_code + " to " + data[i].to_code + " (" + data[i].flight_time + ")"
                    }));
                  }
                });
              }
            }
          });
          // Second ajax request on remove flight
          $("#remove_flight_container_trip").on('change', function() {
            var val = $(this).val();
            $("#remove_flight_container_flight").html("");
            if (val != "") {
              $.get("/trip/" + val + "/flights", function(data) {
                if (data.length == 0) {
                  $("#remove_flight_container_flight").html('<option value="">No flights for this trip</option>');
                }
                for (var i = 0; i < data.length; i++) {
                  $('#remove_flight_container_flight').append($('<option>', {
                    value: data[i].id,
                    text: "From " + data[i].from_airport + " (" + data[i].from_airport_code + ") to " + data[i].to_airport + " (" + data[i].to_airport_code + ")"
                  }));
                }
              });
            }
          });
          // On send request click
          $("#sendRequest").on('click', function() {
            var val = $("#apiTester").val();
            if (val == "") {
              alert("Please choose an option!");
              return;
            }
            $("#results").show();
            $("#results").html("");
            if (val == "list_airports") {
              // Send request list airports
              $.getJSON("/airports", function(data) {
                $("#results").html("<pre><code>" + JSON.stringify(data, null, "\t") + "</code></pre>");
              });
            } else if (val == "list_flights") {
              // Send request list flights
              var tripId = $('#list_flights_container_trips').val();
              $.getJSON("/trip/" + tripId + "/flights", function(data) {
                $("#results").html("<pre><code>" + JSON.stringify(data, null, "\t") + "</code></pre>");
              });
            } else if (val == "add_flight") {
              // Send request add flight
              var fromAirportId = $("#add_flight_container_departure").val();
              var toAirportId = $("#add_flight_container_arrival").val();
              var tripId = $("#add_flight_container_trip").val();
              if (fromAirportId == toAirportId) {
                alert("Departure and arrival airports must be different!");
                $("#results").hide();
                return;
              }
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "JSON",
                type: "POST",
                url: "/trip/" + tripId + "/flight",
                data: { 
                  fromAirportId: fromAirportId,
                  toAirportId: toAirportId 
                }
              }).done(function(data) {
                $("#results").html("<pre><code>" + JSON.stringify(data, null, "\t") + "</code></pre>");
              });
            } else if (val == "remove_flight") {
              // Send request remove flight
              var flightId = $("#remove_flight_container_flight").val();
              if (flightId == "" || flightId == null) {
                alert("You must choose a flight to remove!");
                $("#results").hide();
                return;
              }
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "JSON",
                type: "DELETE",
                url: "/flight/" + flightId
              }).done(function(data) {
                $("#results").html("<pre><code>" + JSON.stringify(data, null, "\t") + "</code></pre>");
                if (data.status == "success") {
                  $("#remove_flight_container_trip").change();
                }
              });
            } else {
              $("#results").hide();
            }
          });
        }
      })(jQuery);
    </script>
</html>
