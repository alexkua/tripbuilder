<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Trip extends Model
{
  /**
   * Get all trips with the airport names
   *
   * @return array containing all trips
   */
  public static function allWithAirportNames() {
    return DB::table('trips')
      ->select('trips.id', 'a1.name as from', 'a1.code as from_code', 'a2.name as to', 'a2.code as to_code', 'trips.flight_time')
      ->join('airports as a1', 'a1.id', '=', 'trips.current_airport_id')
      ->join('airports as a2', 'a2.id', '=', 'trips.destination_airport_id')
      ->get();
  }

  /**
   * Get all flights for the trip
   * 
   * @param  int   $tripId  selected trip
   * @return array list of flights
   */
  public static function allFlights($tripId) {
    return DB::table('flights')
      ->select("flights.*", "a1.name as from_airport", "a1.code as from_airport_code", "a2.name as to_airport", "a2.code as to_airport_code")
      ->join('airports as a1', 'a1.id', '=', 'flights.current_airport_id')
      ->join('airports as a2', 'a2.id', '=', 'flights.destination_airport_id')
      ->where('trip_id', '=', $tripId)
      ->get();
  }

  /**
   * Add a new flight to a trip
   * 
   * @param int $tripId          trip id
   * @param int $fromAirportId   flight from location id
   * @param int $toAirportId     flight to location id
   */
  public static function addFlightToTrip($tripId, $fromAirportId, $toAirportId) {
    DB::table('flights')->insert([
      'trip_id'                => $tripId,
      'current_airport_id'     => $fromAirportId,
      'destination_airport_id' => $toAirportId,
      'created_at'             => date("Y-m-d H:i:s"),
      'updated_at'             => date("Y-m-d H:i:s")
    ]);
    return [
      "status"  => "success",
      "message" => "Flight has successfully been added to trip"
    ];
  }

  /**
   * Remove a flight
   * 
   * @param int $flightId  flightId
   */
  public static function removeFlight($flightId) {
    DB::table('flights')
      ->where('id', '=', $flightId)
      ->delete();
    return [
      "status"  => "success",
      "message" => "Flight has successfully been removed"
    ];
  }
}
