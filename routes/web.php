<?php

use App\Airport;
use App\Trip;
use App\Flight;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/***************/
/* Page routes */
/***************/

Route::get('/', function () {
  return view('welcome');
});

/******************/
/* API GET routes */
/******************/

/**
 * Get all existing trips
 */
Route::get('/trips', function () {
  return Trip::allWithAirportNames();
});

/**
 * Get all flights for trips
 */
Route::get('/trip/{tripId}/flights', function ($tripId) {
  return Trip::allFlights($tripId);
});

/**
 * Get all existing flights
 */
Route::get('/flights', function () {
  return Flight::all();
});

/**
 * Get alphabetical listing of all airports
 */
Route::get('/airports', function () {
  return Airport::orderBy('name')->get();
});

/*******************/
/* API POST routes */
/*******************/

/**
 * Add flight for a trip
 */
Route::post('/trip/{tripId}/flight', function ($tripId) {
  $fromAirportId = Request::input('fromAirportId');
  $toAirportId = Request::input('toAirportId');
  if (empty($fromAirportId)) {
    return [
      'status' => 'error',
      'message' => 'Departure flight id not defined'
    ];
  }
  if (empty($toAirportId)) {
    return [
      'status' => 'error',
      'message' => 'Arrival flight id not defined'
    ];
  }
  return Trip::addFlightToTrip($tripId, $fromAirportId, $toAirportId);
});

/*********************/
/* API DELETE routes */
/*********************/

/**
 * Remove flight from trip
 */
Route::delete('flight/{flightId}', function ($flightId) {
  return Trip::removeFlight($flightId);
});

